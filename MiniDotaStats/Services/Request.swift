//
//  Request.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import Alamofire
import Foundation
import SwiftyJSON

//MARK:
class Request {
  //MARK: Attributes
  static let shared = Request()
  
  private var arrTupActiveRequest: [(req: DataRequest, source: String)] = []
  
  typealias onSuccess = (JSON) -> Void
  typealias onError = (Int, String) -> Void
  typealias onFailure = (String) -> Void
  
  //MARK: Helper
  private func isCanExecuteRequest (for strFor: String) -> Bool {
    if arrTupActiveRequest.firstIndex(where: { (tupActiveRequest) in
      return tupActiveRequest.source == strFor
    }) == nil {
      return true
    }
    
    return false
  }
  
  private func generateRequestHeader() -> HTTPHeaders {
    return HTTPHeaders([
      HTTPHeader.accept("application/json")
    ])
  }
  
  private func createHeroRequest<Parameters: Encodable>(withUrl strUrl: String, method: HTTPMethod, encoder: ParameterEncoder, params: Parameters, header: HTTPHeaders, requestModifier: @escaping ( (inout URLRequest) -> Void)) -> (req: DataRequest?, reqName: String?)?  {
    var request: DataRequest?
    var strRequestName: String?
    
    if let _heroServiceParameters = params as? HeroServices.Parameters {
      strRequestName = isCanExecuteRequest(for: _heroServiceParameters.requestSource) ? _heroServiceParameters.requestSource : nil
      
      if let _paramGetHeroList = _heroServiceParameters.requestGetHeroList {
        request = AF.request(strUrl, method: method, parameters: _paramGetHeroList, encoder: encoder, headers: header, interceptor: nil, requestModifier: requestModifier)
      } else {
        request = AF.request(strUrl, method: method, parameters: params, encoder: encoder, headers: header, interceptor: nil, requestModifier: requestModifier)
      }
      
      return (request, strRequestName)
    }
    
    return nil
  }
  
  //MARK: Cancel
  func cancelRequest(source strSource: String, clearAll isClearAll: Bool = false) {
    if isClearAll {
      for tupActiveRequest in arrTupActiveRequest {
        if tupActiveRequest.source == strSource {
          tupActiveRequest.req.cancel()
        }
      }
      
      arrTupActiveRequest.removeAll(where: { (tupActiveRequest) in
        return tupActiveRequest.source == strSource
      })
    } else {
      if let _intIdxRequest = arrTupActiveRequest.firstIndex(where: { (tupActiveRequest) in
        return tupActiveRequest.source == strSource
      }) {
        let requestAtIdx = arrTupActiveRequest[_intIdxRequest].req
        requestAtIdx.cancel()
        
        arrTupActiveRequest.remove(at: _intIdxRequest)
      }
    }
  }
  
  //MARK: Request / Submit Data
  func requestGetData<Parameters: Encodable>(withUrl strUrl: String, params: Parameters, onSuccess: @escaping onSuccess, onError: @escaping onError, onFailure: @escaping onFailure) {
    let urlFormEncoder = URLEncodedFormEncoder(alphabetizeKeyValuePairs: true, arrayEncoding: .noBrackets, boolEncoding: .literal, dataEncoding: .base64, dateEncoding: .deferredToDate, keyEncoding: .useDefaultKeys, spaceEncoding: .percentEscaped, allowedCharacters: .afURLQueryAllowed)
    let urlParameterEncoder = URLEncodedFormParameterEncoder(encoder: urlFormEncoder, destination: .methodDependent)
    
    requestData(withUrl: strUrl, method: .get, encoder: urlParameterEncoder, params: params, onSuccess: onSuccess, onError: onError, onFailure: onFailure)
  }
  
  func requestData<Parameters: Encodable>(withUrl strUrl: String, method: HTTPMethod, encoder: ParameterEncoder, params: Parameters, onSuccess: @escaping onSuccess, onError: @escaping onError, onFailure: @escaping onFailure) {
    let header = generateRequestHeader()
    
    var request: DataRequest?
    var strRequestSource: String?
    let requestModifier: (inout URLRequest) -> Void = { urlRequest in
      urlRequest.timeoutInterval = 30
    }
    
    if let _tupRequestHero = createHeroRequest(withUrl: strUrl, method: method, encoder: encoder, params: params, header: header, requestModifier: requestModifier) {
      request = _tupRequestHero.req
      strRequestSource = _tupRequestHero.reqName
      
      guard let _req = request,
            let _strRequestSource = strRequestSource else {
        onFailure("Wrong Request")
        
        return
      }
      
      if !Utils.isIPhoneX() {
        if #available(iOS 13, *) {
          //TODO:
        } else {
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
      }
      
      arrTupActiveRequest.append((_req, _strRequestSource))
      
      _req.responseJSON(completionHandler: { (response) in
        switch response.result {
          case let .success(value):
            let jsonValue = JSON(value)
            
            if jsonValue.arrayValue.count < 0 {
              onError(-1, "Not enough data that can be displayed")
            } else {
              onSuccess(jsonValue)
            }
          case let .failure(err):
            guard let _errNative = err.underlyingError else {
              onFailure(err.localizedDescription)
              
              return
            }

            onFailure(_errNative.localizedDescription)
        }
        
        self.cancelRequest(source: _strRequestSource)
        
        if self.arrTupActiveRequest.count == 0 {
          if !Utils.isIPhoneX() {
            if #available(iOS 13, *) {
              //TODO:
            } else {
              UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
          }
        }
      })
    } else {
      onFailure("Wrong Request")
    }
  }
}


