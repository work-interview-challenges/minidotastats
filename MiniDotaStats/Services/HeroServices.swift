//
//  HeroesServices.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import Foundation
import SwiftyJSON

//MARK:
class HeroServices {
  //Struct
  struct Parameters: Encodable {
    //MARK: Attributes
    var requestName: String
    var requestSource: String
    
    var requestGetHeroList: GetHeroListParameters?
  
    //MARK: Initialization
    init(from strFrom: String, name strName: String) {
      requestName = strName
      requestSource = "\(strName)_\(strFrom)"
    }
  }
  
  struct GetHeroListParameters: Encodable {}
  
  //MARK: Attributes
  static let shared = HeroServices()
  
  static let API_NAME_GETHEROLIST = "HeroServices_GetHeroList"

  typealias onGetHeroListSuccess = (_ heroes: [Hero], _ roles: [String]) -> Void
  
  //MARK: Helper
  func cancel(from strFrom: String, name strName: String, clearAll isClearAll: Bool = false) {
    let params = Parameters(from: strFrom, name: strName)
    
    Request.shared.cancelRequest(source: params.requestSource, clearAll: isClearAll)
  }
  
  //MARK: - API
  func getHeroList(from strFrom: String, onSuccess: @escaping onGetHeroListSuccess, onError: @escaping Request.onError, onFailure: @escaping Request.onFailure) {
    var params = Parameters(from: strFrom, name: HeroServices.API_NAME_GETHEROLIST)
    params.requestGetHeroList = nil//GetHeroListParameters()
    
    let strUrlGetHeroList = "\(Constants.API_BASE_URL)/herostats"
    
    Request.shared.requestGetData(withUrl: strUrlGetHeroList, params: params, onSuccess: { (response) in
      Utils.setHeroData(withValue: (response.rawString() ?? "").data(using: .utf8) ?? Data())
      
      if let _tupData = Utils.generateHeroData(forKey: .listingData) {
        onSuccess(_tupData.heroes, _tupData.roles)
      } else {
        onError(-1, "Data cannot be found")
      }
    }, onError: onError, onFailure: onFailure)
  }
}

