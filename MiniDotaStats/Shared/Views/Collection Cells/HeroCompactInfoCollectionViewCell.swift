//
//  HeroesCompactInfoCollectionViewCell.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 22/04/21.
//

import UIKit
import Kingfisher

//MARK: - Class Declaration
class HeroCompactInfoCollectionViewCell: UICollectionViewCell {
  //MARK: Outlets
  @IBOutlet private weak var imgHero: UIImageView!
  @IBOutlet private weak var lblName: UILabel!
  
  //MARK: Constraints
  @IBOutlet private weak var consLowBotImageToSuperview: NSLayoutConstraint!
  @IBOutlet private weak var consHighBotImageToTopLblName: NSLayoutConstraint!
  
  //MARK: Attributes
  static let identifier = String(describing: HeroCompactInfoCollectionViewCell.self)
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    
    imgHero.kf.indicatorType = .activity
  }
  
  //MARK: Setup
  func setUI(withImage strImage: String) {
    setUI(withImage: strImage, name: "")
  }
  
  func setUI(withImage strImage: String, name strName: String) {
    //https://steamcdn-a.akamaihd.net/apps/dota2/images/heroes/antimage_full.png?
    let url = URL(string: "https://steamcdn-a.akamaihd.net/\(strImage)")
    imgHero.kf.setImage(with: url)
    
    if strName.isEmpty {
      imgHero.contentMode = .scaleAspectFill
      
      consLowBotImageToSuperview.priority = UILayoutPriority.defaultHigh + 50
      consHighBotImageToTopLblName.priority = UILayoutPriority.defaultLow - 50
    } else {
      imgHero.contentMode = .scaleAspectFit
      
      consLowBotImageToSuperview.priority = UILayoutPriority.defaultLow - 50
      consHighBotImageToTopLblName.priority = UILayoutPriority.defaultHigh + 50
    }
    
    lblName.text = strName
  }
}
