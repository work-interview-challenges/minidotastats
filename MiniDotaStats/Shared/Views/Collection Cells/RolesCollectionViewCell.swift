//
//  RolesCollectionViewCell.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 22/04/21.
//

import UIKit

//MARK: - Class Declaration
class RolesCollectionViewCell: UICollectionViewCell {
  //MARK: Outlets
  @IBOutlet private weak var lblRoles: UILabel!
  
  //MARK: Attributes
  static let identifier = String(describing: RolesCollectionViewCell.self)
  
  override var isSelected: Bool {
    didSet {
      updateSelectedStatus()
    }
  }
  
  //MARK: Helper
  private func updateSelectedStatus() {
    if isSelected {
      lblRoles.textColor = UIColor.green
    } else {
      lblRoles.textColor = UIColor.white
    }
  }
  
  func setUI(withValue strValue: String) {
    lblRoles.layer.cornerRadius = 10
    
    lblRoles.text = strValue
  }
}
