//
//  HeaderView.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK:
@objc protocol HeaderViewDelegate: NSObjectProtocol {
  func actBack()
}

//MARK:
class HeaderView: BaseViewXIB {
  //MARK: - Outlets
  @IBOutlet public weak var delegate: HeaderViewDelegate?

  @IBOutlet private weak var imgVwBack: UIImageView!
  @IBOutlet private weak var btnBack: UIButton!
  @IBOutlet private weak var lblTitle: UILabel!
  
  //MARK: Actions
  @IBAction private func actBack(_ sender: UIButton) {
    delegate?.actBack()
  }

  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    
    resetUI()
  }
  
  //MARK: Helper
  func resetUI() {
    imgVwBack.isHidden = true
    btnBack.isEnabled = !imgVwBack.isHidden
    
    lblTitle.text = nil
  }
  
  //MARK: Setup
  func getTitle() -> String {
    return lblTitle.text ?? ""
  }
  
  func setUI(withTitle strTitle: String) {
    setUI(withTitle: strTitle, canBack: false)
  }
  
  func setUI(withTitle strTitle: String, canBack isCanBack: Bool) {
    lblTitle.text = strTitle
    
    if isCanBack {
      imgVwBack.isHidden = false
    } else {
      imgVwBack.isHidden = true
    }
    
    btnBack.isEnabled = !imgVwBack.isHidden
  }
}

