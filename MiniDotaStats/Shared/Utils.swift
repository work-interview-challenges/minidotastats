//
//  Utils.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import SwiftyJSON
import UIKit

//MARK:
struct Utils {
  //MARK: - Flags
  static func isIPhoneX() -> Bool {
    let edgInsSafeArea = getSafeAreaInsets()
    
    return edgInsSafeArea.top != 0 && edgInsSafeArea.bottom != 0
  }
  
  //MARK: - Screen
  static func getSafeAreaInsets() -> UIEdgeInsets {
    let winMain = getMainWindow()
    
    return winMain.safeAreaInsets
  }
  
  static func getMainWindow() -> UIWindow {
    let app = UIApplication.shared
    
    if #available(iOS 13.0, *) {
      if let _sceneMain = app.connectedScenes.first,
         let _del = _sceneMain.delegate as? SceneDelegate,
         let _winMain = _del.window {
        return _winMain
      }
    } else {
      if let _del = app.delegate,
         let _win = _del.window,
         let _winMain = _win {
        return _winMain
      }
    }
    
    return UIWindow(frame: UIScreen.main.bounds)
  }
  
  //MARK: - Defaults
  static func setHeroData(withValue dtValue: Data) {
    UserDefaults.heroData?.setValue(dtValue, forKey: UserDefaults.HeroDataKey.listingData.rawValue)
  }
  
  static func generateHeroData(forKey: UserDefaults.HeroDataKey) -> (heroes: [Hero], roles: [String])? {
    var arrHeroList = [Hero]()
    var arrRoles = [String]()
    
    if let dt = UserDefaults.heroData?.value(forKey: forKey.rawValue) as? Data,
       let response = try? JSON(data: dt),
       let _arrResp = response.array {
    
      let decoder = JSONDecoder()
      
      for resp in _arrResp {
        if let _strJSONData = resp.rawString(.utf8),
           let _dtStrJSONData = _strJSONData.data(using: .utf8) {
          do {
            let hero = try decoder.decode(Hero.self, from: _dtStrJSONData)
            
            arrHeroList.append(hero)
            arrRoles.mergeWith(content: hero.roles)
          } catch {
            continue
          }
        } else {
          continue
        }
      }
    }
    
    if arrHeroList.count == 0 || arrRoles.count == 0 {
      return nil
    } else {
      return (arrHeroList, arrRoles)
    }
  }
  
  //MARK: - Alert
  static func showErrorAlert(from: UIViewController, description strDesc: String?, code intCode: Int, handler: ((UIAlertAction) -> Void)? = nil) {
    showAlert(from: from, description: strDesc, code: intCode, userInfo: nil, actTitle: Bundle.localizedCommonString(forKey: "Ok").uppercased(), handler: handler)
  }
  
  static func showReloadAlert(from: UIViewController, description strDesc: String?, code intCode: Int, handler: ((UIAlertAction) -> Void)?) {
    showAlert(from: from, description: strDesc, code: intCode, userInfo: nil, actTitle: Bundle.localizedCommonString(forKey: "Reload").uppercased(), handler: handler)
  }
  
  static func showAlert(from: UIViewController, description strDesc: String?, code intCode: Int, userInfo dictUserInfo: [String: Any]?, actTitle strActTitle: String, handler: ((UIAlertAction) -> Void)?) {
    if intCode != 0 {
      var strMessage = strDesc
      
      if let _dictUserInfo = dictUserInfo,
         let _strLocalDesc = _dictUserInfo[NSLocalizedDescriptionKey] as? String {
        strMessage = _strLocalDesc
      }
    
      let actDefault = UIAlertAction(title: strActTitle, style: .cancel, handler: handler)
      
      let vcAlert = UIAlertController(title: Bundle.localizedCommonString(forKey: "Error", table: "Common"), message: strMessage, preferredStyle: .alert)
      vcAlert.addAction(actDefault)
      
      from.present(vcAlert, animated: true, completion: nil)
    }
  
    return
  }
}

