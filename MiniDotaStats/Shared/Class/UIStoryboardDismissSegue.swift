//
//  UIStoryboardDismissSegue.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK:
class UIStoryboardDismissSegue: UIStoryboardSegue {
  //MARK: Attributes
  var completion: (() -> Void)?
  
  //MARK: Action
  override func perform() {
    if let _vcNav = source.parent as? UINavigationController,
       let _isFirstChild = _vcNav.children.first?.isKind(of: source.classForCoder) {
      if _isFirstChild && source.presentingViewController != nil {
        source.dismiss(animated: true, completion: completion)
      } else {
        if completion != nil {
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: completion!)
        }
        
        _vcNav.popViewController(animated: true)
      }
    } else if let _vcPresenting = source.presentingViewController {
      _vcPresenting.dismiss(animated: true, completion: completion)
    }
  }
}

