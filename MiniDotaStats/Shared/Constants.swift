//
//  Constants.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import Foundation

//MARK: -
struct Constants {
  //MARK: - Server Configuration
  #if DEBUG
  static let API_BASE_URL = "https://api.opendota.com/api"
  #else
  static let API_BASE_URL = "https://api.opendota.com/api"
  
  #endif
}
