//
//  Array+MiniDotaStats.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 22/04/21.
//

import Foundation

extension Array where Element: Equatable {
  mutating func mergeWith<C: Collection>(content: C) where C.Iterator.Element == Element {
    let arrFilteredContent = content.filter({!contains($0)})
    append(contentsOf: arrFilteredContent)
  }
}
