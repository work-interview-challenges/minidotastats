//
//  Defaults+MiniDotaStats.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 25/04/21.
//

import Foundation

//MARK: -
extension UserDefaults {
  //MARK: Enum
  enum HeroDataKey: String {
    case listingData
  }
  
  //MARK: Attributes
  static let requestDelay = UserDefaults(suiteName: "request_delay")
  static let heroData = UserDefaults(suiteName: "hero_data")
}
