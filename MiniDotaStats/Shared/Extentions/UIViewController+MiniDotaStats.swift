//
//  UIViewController+MiniDotaStats.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK:
extension UIViewController {
  enum SegueIdentifier: String {
    case showHeroDetailController
    case dismissHeroDetailController
  }
}
