//
//  Bundle+MiniDotaStats.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import Foundation

//MARK:
extension Bundle {
  static func localizedString(forKey strForKey: String, value strValue: String? = nil) -> String {
    return Bundle.localizedCommonString(forKey: strForKey, value: strValue, table: nil)
  }
  
  static func localizedCommonString(forKey strForKey: String, value strValue: String? = nil, table strTable: String? = "Common") -> String {
    return Bundle.main.localizedString(forKey: strForKey, value: strValue ?? strForKey, table: strTable)
  }
}
