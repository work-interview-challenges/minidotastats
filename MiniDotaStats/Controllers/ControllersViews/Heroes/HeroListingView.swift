//
//  HeroListingView.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK: - Delegate
protocol HeroListingViewDelegate: NSObjectProtocol {
  func actShowHeroDetail(title: String, selected: Hero, similar: [Hero])
}

//MARK: - Class Declaration
class HeroListingView: BaseViewXIB {
  //MARK: Outlets
  public weak var delegate: HeroListingViewDelegate?
  
  @IBOutlet private weak var vwHeader: HeaderView!
  
  @IBOutlet private weak var colVwRoles: UICollectionView!
  @IBOutlet private weak var colVwHeroes: UICollectionView!
  
  @IBOutlet private weak var actIndListing: UIActivityIndicatorView!
  
  //MARK: Attributes
  private var arrSelectedRoles: [String] = []
  private var tupData: (roles: [String], heroes: [Hero], heroesFiltered: [Hero]) = ([String](), [Hero](), [Hero]())
  private var tupCellData: (size: CGSize, lineSpacing: CGFloat, insets: UIEdgeInsets) = (CGSize.zero, 0.0, UIEdgeInsets.zero)
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    
    vwHeader.setUI(withTitle: "Reloading Data")
    
    let nibRolesCell = UINib(nibName: RolesCollectionViewCell.identifier, bundle: nil)
    colVwRoles.register(nibRolesCell, forCellWithReuseIdentifier: RolesCollectionViewCell.identifier)
    colVwRoles.layer.borderWidth = 1.0
    colVwRoles.layer.borderColor = UIColor(white: 0.0, alpha: 0.5).cgColor
    colVwRoles.isHidden = true
    
    let nibHeroesCell = UINib(nibName: HeroCompactInfoCollectionViewCell.identifier, bundle: nil)
    colVwHeroes.register(nibHeroesCell, forCellWithReuseIdentifier: HeroCompactInfoCollectionViewCell.identifier)
    colVwHeroes.isHidden = true
    
    actIndListing.startAnimating()
  }
  
  //MARK: Helper
  private func getCellSize(from layFrom: UICollectionViewLayout) -> (CGSize, CGFloat, UIEdgeInsets) {
    let szScreen = UIScreen.main.bounds
    let szScreenRatioWithBase = szScreen.height / 568.0
    
    var szCell = CGSize(width: 200, height: 200)
    var fltCellSpacing = CGFloat(15.0)
    var fltLineSpacing = CGFloat(20.0)
    var edsIns = UIEdgeInsets.zero
    
    guard let _layFrom = layFrom as? UICollectionViewFlowLayout else {
      return (szCell, fltLineSpacing, edsIns)
    }
    
    if szScreen.height > 568.0 {
      fltCellSpacing = _layFrom.minimumInteritemSpacing * szScreenRatioWithBase
      fltLineSpacing = _layFrom.minimumLineSpacing * szScreenRatioWithBase
      edsIns = _layFrom.sectionInset
      
      let intMaxColumn = 3
      
      let fltWidthCellReserved = edsIns.left + edsIns.right + (CGFloat(intMaxColumn - 1) * fltCellSpacing)
      let fltWidthCellAvailable = floor((szScreen.width - fltWidthCellReserved) / CGFloat(intMaxColumn))
      let fltHeightCellAvailable = fltWidthCellAvailable * 1.0
      
      szCell = CGSize(width: fltWidthCellAvailable, height: fltHeightCellAvailable)
    }

    return (szCell, fltLineSpacing, edsIns)
  }
  
  private func filterHeroes() {
    if arrSelectedRoles.count == 0 || arrSelectedRoles.count == tupData.roles.count {
      vwHeader.setUI(withTitle: "All")
      
      tupData.heroesFiltered = tupData.heroes
    } else {
      if arrSelectedRoles.count == 1 {
        vwHeader.setUI(withTitle: arrSelectedRoles.first!)
      } else if arrSelectedRoles.count > 1 {
        vwHeader.setUI(withTitle: "\(arrSelectedRoles.count) roles selected")
      }
      
      let heroesFilteredNewest = tupData.heroes.filter { (hero) -> Bool in
        var isFitRole = false
        
        for roles in arrSelectedRoles {
          if !isFitRole && hero.roles.contains(roles) {
            isFitRole = !isFitRole
            
            break
          }
        }
        
        return isFitRole
      }
      
      tupData.heroesFiltered = heroesFilteredNewest
    }
    

    colVwRoles.reloadData()
    colVwHeroes.reloadData()
  }
  
  //MARK: Setup
  func setUI(withRoles arrRoles: [String], heroes arrHeroes: [Hero]) {
    vwHeader.setUI(withTitle: "All")
    
    tupData = (arrRoles, arrHeroes, arrHeroes)
    
    actIndListing.stopAnimating()
    
    colVwRoles.isHidden = false
    colVwRoles.reloadData()
    
    colVwHeroes.isHidden = false
    colVwHeroes.reloadData()
  }
  
  func setUIViewDidLayoutSubviews() {
    if tupCellData.size.equalTo(CGSize.zero) {
      tupCellData = getCellSize(from: colVwHeroes.collectionViewLayout)
      
      colVwHeroes.reloadData()
    }
  }
}

//MARK: - UICollectionViewDataSource
extension HeroListingView: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView.isEqual(colVwRoles) {
      return tupData.roles.count
    } else {
      return tupData.heroesFiltered.count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView.isEqual(colVwRoles) {
      guard let _cellRoles = collectionView.dequeueReusableCell(withReuseIdentifier: RolesCollectionViewCell.identifier, for: indexPath) as? RolesCollectionViewCell else {
        return UICollectionViewCell()
      }
      
      let roleAtIdxPath = tupData.roles[indexPath.row]
      
      _cellRoles.setUI(withValue: roleAtIdxPath)
      _cellRoles.isSelected = arrSelectedRoles.contains(roleAtIdxPath)
      
      return _cellRoles
    } else {
      guard let _cellHero = collectionView.dequeueReusableCell(withReuseIdentifier: HeroCompactInfoCollectionViewCell.identifier, for: indexPath) as? HeroCompactInfoCollectionViewCell else {
        return UICollectionViewCell()
      }
      
      let heroAtIdxPath = tupData.heroesFiltered[indexPath.row]
      
      _cellHero.setUI(withImage: heroAtIdxPath.image, name: heroAtIdxPath.displayName)
      
      return _cellHero
    }
  }
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

//MARK: - UICollectionViewDelegate
extension HeroListingView: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView.isEqual(colVwRoles) {
      let rolesSelected = tupData.roles[indexPath.row]
      
      if arrSelectedRoles.contains(rolesSelected) {
        arrSelectedRoles.removeAll{($0 == rolesSelected)}
      } else {
        arrSelectedRoles.append(rolesSelected)
      }
      
      filterHeroes()
    } else {
      let heroSelected = tupData.heroesFiltered[indexPath.row]
      let strTitle = vwHeader.getTitle()
      
      var arrHeroWithoutSelected = tupData.heroes.filter { (hero) -> Bool in
        return hero.displayName != heroSelected.displayName && hero.primaryAttribute == heroSelected.primaryAttribute
      }
      
      switch Hero.AttributeType(rawValue: heroSelected.primaryAttribute) {
        case .str:
          let arrHeroWithMaxBaseAtk = arrHeroWithoutSelected.sorted { (heroPrev, heroNext) -> Bool in
            return heroPrev.baseAtkMax > heroNext.baseAtkMax
          }
          
          var arrTopThreeWithMaxBaseAtk: [Hero] = []
          
          if arrHeroWithMaxBaseAtk.count > 3 {
            arrTopThreeWithMaxBaseAtk = Array(arrHeroWithMaxBaseAtk[0...2])
          } else {
            arrTopThreeWithMaxBaseAtk = arrHeroWithMaxBaseAtk
          }
          
          delegate?.actShowHeroDetail(title: strTitle, selected: heroSelected, similar: arrTopThreeWithMaxBaseAtk)
        case .agi:
          let arrHeroWithMaxMoveSpeed = arrHeroWithoutSelected.sorted { (heroPrev, heroNext) -> Bool in
            return heroPrev.baseMoveSpeed > heroNext.baseMoveSpeed
          }
          
          var arrTopThreeHeroWithMaxMoveSpeed: [Hero] = []
          
          if arrHeroWithMaxMoveSpeed.count > 3 {
            arrTopThreeHeroWithMaxMoveSpeed = Array(arrHeroWithMaxMoveSpeed[0...2])
          } else {
            arrTopThreeHeroWithMaxMoveSpeed = arrHeroWithMaxMoveSpeed
          }
          
          delegate?.actShowHeroDetail(title: strTitle, selected: heroSelected, similar: arrTopThreeHeroWithMaxMoveSpeed)
        case .int:
          let arrHeroWithMaxMana = arrHeroWithoutSelected.sorted { (heroPrev, heroNext) -> Bool in
            return heroPrev.baseMana > heroNext.baseMana
          }
          
          var arrTopThreeHeroWithMaxMana: [Hero] = []
          
          if arrHeroWithMaxMana.count > 3 {
            arrTopThreeHeroWithMaxMana = Array(arrHeroWithMaxMana[0...2])
          } else {
            arrTopThreeHeroWithMaxMana = arrHeroWithMaxMana
          }
          
          delegate?.actShowHeroDetail(title: strTitle, selected: heroSelected, similar: arrTopThreeHeroWithMaxMana)
        default:
          break
      }
    }
  }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension HeroListingView: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    switch collectionView {
      case colVwRoles:
        return CGSize(width: 100, height: colVwRoles.frame.height)
      default:
        return tupCellData.size
    }
  }
    
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    switch collectionView {
      case colVwRoles:
        return (collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing
      default:
        return tupCellData.lineSpacing
    }
  }
}
