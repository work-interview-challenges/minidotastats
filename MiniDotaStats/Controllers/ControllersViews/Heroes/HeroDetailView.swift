//
//  HeroDetailView.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK: - Delegate
@objc protocol HeroDetailViewDelegate: NSObjectProtocol {
  func actBack()
}

//MARK: - Class Declaration
class HeroDetailView: BaseViewXIB {
  //MARK: Outlets
  @IBOutlet public weak var delegate: HeroDetailViewDelegate?
  
  @IBOutlet private weak var vwHeader: HeaderView!
  @IBOutlet private weak var imgVwHeroImage: UIImageView!
  
  @IBOutlet private weak var vwDetailContainer: UIView!
  @IBOutlet private weak var lblHeroName: UILabel!
  @IBOutlet private weak var lblAtkTypeValue: UILabel!
  @IBOutlet private weak var lblHeroBaseDamageValue: UILabel!
  @IBOutlet private weak var lblHeroArmorValue: UILabel!
  @IBOutlet private weak var lblHeroMoveSpeedValue: UILabel!
  @IBOutlet private weak var lblHeroHealthValue: UILabel!
  @IBOutlet private weak var lblHeroManaValue: UILabel!
  @IBOutlet private weak var lblHeroMainAttributeValue: UILabel!
  @IBOutlet private weak var lblHeroRolesValue: UILabel!
  @IBOutlet private weak var colVwSimilarHeroes: UICollectionView!
  
  //MARK: Constraints
  @IBOutlet private weak var consSuperHighColVwSimilar: NSLayoutConstraint!
  
  //MARK: Attributes
  private var tupData: (hero: Hero, heroSimilar: [Hero]) = (try! JSONDecoder().decode(Hero.self, from: "{}".data(using: .utf8)!), [Hero]())
  private var tupCellData: (size: CGSize, lineSpacing: CGFloat, insets: UIEdgeInsets) = (CGSize.zero, 0.0, UIEdgeInsets.zero)
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    
    vwHeader.setUI(withTitle: "-", canBack: true)

    let nibHeroesCell = UINib(nibName: HeroCompactInfoCollectionViewCell.identifier, bundle: nil)
    colVwSimilarHeroes.register(nibHeroesCell, forCellWithReuseIdentifier: HeroCompactInfoCollectionViewCell.identifier)
  }
  
  //MARK: Helper
  private func getCellSize(from layFrom: UICollectionViewLayout) -> (CGSize, CGFloat, UIEdgeInsets) {
    let szScreen = UIScreen.main.bounds
    
    var szCell = CGSize(width: 200, height: 100)
    var fltCellSpacing = CGFloat(0.0)
    var fltLineSpacing = CGFloat(15.0)
    var edsIns = UIEdgeInsets.zero
    
    guard let _layFrom = layFrom as? UICollectionViewFlowLayout else {
      return (szCell, fltLineSpacing, edsIns)
    }
    
    if szScreen.height > 568.0 {
      fltCellSpacing = _layFrom.minimumInteritemSpacing
      fltLineSpacing = _layFrom.minimumLineSpacing
      edsIns = _layFrom.sectionInset
      
      let intMaxColumn = 3
      
      let fltWidthCellReserved = edsIns.left + edsIns.right + (CGFloat(intMaxColumn - 1) * fltCellSpacing)
      let fltWidthCellAvailable = floor((szScreen.width - fltWidthCellReserved) / CGFloat(intMaxColumn))
      let fltHeightCellAvailable = fltWidthCellAvailable * 0.5
      
      szCell = CGSize(width: fltWidthCellAvailable, height: fltHeightCellAvailable)
    }

    return (szCell, fltLineSpacing, edsIns)
  }
  
  //MARK: Setup
  func setUI(withTitle strTitle: String, forHero hero: Hero, similarHero arrSimilarHero: [Hero]) {
    vwHeader.setUI(withTitle: strTitle, canBack: true)
    
    tupData = (hero, arrSimilarHero)

    let url = URL(string: "https://steamcdn-a.akamaihd.net/\(hero.image)")
    imgVwHeroImage.kf.setImage(with: url)
    
    lblHeroName.text = tupData.hero.displayName
    
    lblAtkTypeValue.text = tupData.hero.attackType
    lblHeroBaseDamageValue.text = "\(tupData.hero.baseAtkMin) - \(tupData.hero.baseAtkMax)"
    lblHeroArmorValue.text = "\(CGFloat(tupData.hero.baseArmor))"
    lblHeroMoveSpeedValue.text = "\(tupData.hero.baseMoveSpeed)"
    lblHeroHealthValue.text = "\(tupData.hero.baseHealth)"
    lblHeroManaValue.text = "\(tupData.hero.baseMana)"
    lblHeroMainAttributeValue.text = "\(tupData.hero.primaryAttribute)"
    lblHeroRolesValue.text = tupData.hero.rolesDisplayed
      
    colVwSimilarHeroes.reloadData()
  }
  
  func setUIViewDidLayoutSubviews() {
    if tupCellData.size.equalTo(CGSize.zero) {
      tupCellData = getCellSize(from: colVwSimilarHeroes.collectionViewLayout)
      
      consSuperHighColVwSimilar.constant = tupCellData.size.height
    
      colVwSimilarHeroes.reloadData()
    }
  }
}

//MARK: HeaderViewDelegate
extension HeroDetailView: HeaderViewDelegate {
  func actBack() {
    delegate?.actBack()
  }
}

//MARK: - UICollectionViewDataSource
extension HeroDetailView: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return tupData.heroSimilar.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let _cellHero = collectionView.dequeueReusableCell(withReuseIdentifier: HeroCompactInfoCollectionViewCell.identifier, for: indexPath) as? HeroCompactInfoCollectionViewCell else {
      return UICollectionViewCell()
    }
    
    let heroAtIdxPath = tupData.heroSimilar[indexPath.row]
    
    _cellHero.setUI(withImage: heroAtIdxPath.image)
    
    return _cellHero
  }
    
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
}

//MARK: - UICollectionViewDelegate
extension HeroDetailView: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
}

//MARK: - UICollectionViewDelegateFlowLayout
extension HeroDetailView: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return tupCellData.size
  }
    
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return tupCellData.lineSpacing
  }
}

