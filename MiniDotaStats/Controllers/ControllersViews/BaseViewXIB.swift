//
//  BaseViewXIB.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK:
class BaseViewXIB: UIView {
  //MARK: Initialization
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    
    let strVwName = String(describing: type(of: self))
    
    guard let _xib = Bundle.main.loadNibNamed(strVwName, owner: self, options: nil) as? [UIView],
          _xib.count > 0 else {
      return
    }
    
    let xib = _xib as [AnyObject]
    var vwSelected: UIView?
    
    for obj in xib {
      if obj.isKind(of: UIView.classForCoder()) {
        vwSelected = obj as? UIView
        
        break
      }
    }
    
    guard let _vwSelected = vwSelected else {
      return
    }
    
    _vwSelected.frame = bounds
    
    addSubview(_vwSelected)
  }
  
  //MARK: Attributes
  var txtFieldActive: UITextField?
  
  var tapGesDismissKeyboard: UITapGestureRecognizer?
  
  var isDismissKeyboardByTapBackground = false {
    didSet {
      addOrRemoveGestureDismissKeyboard()
    }
  }
  
  //MARK: Gestures
  @objc private func tapGesDismissKeyboard(gesture: UIGestureRecognizer) {
    switch gesture.state {
      case .ended:
        txtFieldActive?.resignFirstResponder()
      default:
        break
    }
  }
  
  //MARK: Helper
  func addOrRemoveGestureDismissKeyboard() {
    if isDismissKeyboardByTapBackground {
      if tapGesDismissKeyboard == nil {
        tapGesDismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapGesDismissKeyboard(gesture:)))
        
        addGestureRecognizer(tapGesDismissKeyboard!)
      } else {
        tapGesDismissKeyboard = nil
      }
    }
  }
}
