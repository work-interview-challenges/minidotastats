//
//  BaseController.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK:
class BaseController: UIViewController {
  //MARK: Attributes
  var strVCName = ""
  
  var rectKeyboard = CGRect.zero
  
  //MARK: Keyboard
  private func getKeyboardRect(notification: Notification) -> CGRect {
    guard let _dictInfo = notification.userInfo,
          let _rectKeyboard = _dictInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
      return CGRect.zero
    }
    
    return _rectKeyboard
  }
  
  @objc
  func keyboardWillShow(notification: Notification) {
    rectKeyboard = getKeyboardRect(notification: notification)
  }
  
  @objc
  func keyboardWillHide(notification: Notification) {
    rectKeyboard = getKeyboardRect(notification: notification)
  }
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setControllerName()
    setMainView()
  }
    
  //MARK: Setup
  private func setHeroView(withClassName strClassName: String) {
    if strClassName.contains("HeroListing") {
      (self as! HeroListingController).vwMain = (view as! HeroListingView)
    } else if strClassName.contains("HeroDetail") {
      (self as! HeroDetailController).vwMain = (view as! HeroDetailView)
    }
  }
  
  func setControllerName() {
    strVCName = String(describing: type(of: self))
  }
  
  private func setMainView() {
    if strVCName.contains("Hero") {
      setHeroView(withClassName: strVCName)
    }
  }
}
