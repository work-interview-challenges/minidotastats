//
//  HeroesListingController.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK: - Class Declaration
class HeroListingController: BaseController {
  //MARK: Attributes
  var vwMain: HeroListingView?
    
  //MARK: Attributes Navigation
  private var tupHeroData: (title: String, selected: Hero, similar: [Hero]) = ("", try! JSONDecoder().decode(Hero.self, from: "{}".data(using: .utf8)!), [Hero]())
    
  //MARK: Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    
    if let _vcHeroDetail = segue.destination as? HeroDetailController {
      _vcHeroDetail.tupHeroData = self.tupHeroData
    }
  }
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    vwMain?.delegate = self
    
    if let _tupData = Utils.generateHeroData(forKey: .listingData) {
      vwMain?.setUI(withRoles: _tupData.roles, heroes: _tupData.heroes)
      
      getListingData()
    } else {
      getListingData()
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    vwMain?.setUIViewDidLayoutSubviews()
  }
  
  //MARK: Setup
  func getListingData() {
    HeroServices.shared.getHeroList(from: strVCName, onSuccess: { [self] (heroes, roles) in
      vwMain?.setUI(withRoles: roles, heroes: heroes)
    }, onError: { (code, message) in
      Utils.showErrorAlert(from: self, description: message, code: code)
    }, onFailure: { (message) in
      Utils.showErrorAlert(from: self, description: message, code: -1)
    })
  }
}

//MARK: - HeroListingViewDelegate
extension HeroListingController: HeroListingViewDelegate {
  func actShowHeroDetail(title: String, selected: Hero, similar: [Hero]) {
    tupHeroData = (title, selected, similar)
    
    performSegue(withIdentifier: UIViewController.SegueIdentifier.showHeroDetailController.rawValue, sender: self)
  }
}
