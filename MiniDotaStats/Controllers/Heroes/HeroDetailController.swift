//
//  HeroDetailController.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import UIKit

//MARK: - Class Declaration
class HeroDetailController: BaseController {
  //MARK: Attributes
  var vwMain: HeroDetailView?
  
  //MARK: Attributes Navigation
  var tupHeroData: (title: String, selected: Hero, similar: [Hero]) = ("", try! JSONDecoder().decode(Hero.self, from: "{}".data(using: .utf8)!), [Hero]())
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    vwMain?.setUI(withTitle: tupHeroData.title, forHero: tupHeroData.selected, similarHero: tupHeroData.similar)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    vwMain?.setUIViewDidLayoutSubviews()
  }
}


//MARK: - HeroDetailViewDelegate
extension HeroDetailController: HeroDetailViewDelegate {
  func actBack() {
    performSegue(withIdentifier: UIViewController.SegueIdentifier.dismissHeroDetailController.rawValue, sender: self)
  }
}
