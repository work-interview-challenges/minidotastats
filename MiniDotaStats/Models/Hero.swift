//
//  Heroes.swift
//  MiniDotaStats
//
//  Created by Daniel Yanuar Sebastian on 21/04/21.
//

import Foundation

//MARK: -
struct Hero: Codable {
  //MARK: - Enum
  enum AttributeType: String {
    case str
    case agi
    case int
  }
  
  private enum CodingKeys: String, CodingKey {
    case id = "id"
    case idHero = "hero_id"
    
    case totalBanInPro = "pro_ban"
    case totalProPick = "pro_pick"
    case totalProWin = "pro_win"
    case totalTurboPick = "turbo_picks"
    
    case baseAtkMin = "base_attack_min"
    case baseAtkMax = "base_attack_max"
    case baseStr = "base_str"
    case baseAgi = "base_agi"
    case baseInt = "base_int"
    case baseHealth = "base_health"
    case baseMana = "base_mana"
    case baseManaRegen = "base_mana_regen"
    case baseArmor = "base_armor"
    case baseMoveSpeed = "move_speed"
    case baseAtkRange = "attack_range"
    case baseProjectileSpeed = "projectile_speed"
    
    case baseHealthRegen = "base_health_regen"
    case baseTurnRate = "turn_rate"
    case baseAttackRate = "attack_rate"
    
    case strGain = "str_gain"
    case agiGain = "agi_gain"
    case intGain = "int_gain"
    
    case rawName = "name"
    case displayName = "localized_name"
    case icon
    case image = "img"
    case attackType = "attack_type"
    case primaryAttribute = "primary_attr"
    
    case roles
  }
  
  //MARK: Attributes
  let id: Int
  let idHero: Int
  
  let totalBanInPro: Int
  let totalProPick: Int
  let totalProWin: Int
  let totalTurboPick: Int
  
  let baseAtkMin: Int
  let baseAtkMax: Int
  let baseStr: Int
  let baseAgi: Int
  let baseInt: Int
  let baseHealth: Int
  let baseMana: Int
  let baseManaRegen: Int
  let baseArmor: Int
  let baseMoveSpeed: Int
  let baseAtkRange: Int
  let baseProjectileSpeed: Int
  
  let strGain: Double
  let agiGain: Double
  let intGain: Double
  
  let baseHealthRegen: Double
  let baseTurnRate: Double
  let baseAttackRate: Double
  
  let rawName: String
  let displayName: String
  let icon: String
  let image: String
  let attackType: String
  let primaryAttribute: String
  
  let roles: [String]
  let rolesDisplayed: String
  
  //MARK: Lifecycle
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    do { self.id = try container.decode(Int.self, forKey: .id) }
    catch { self.id = 0 }
    do { self.idHero = try container.decode(Int.self, forKey: .idHero) }
    catch { self.idHero = 0 }
    
    do { self.totalBanInPro = try container.decode(Int.self, forKey: .totalBanInPro) }
    catch { self.totalBanInPro = 0 }
    do { self.totalProPick = try container.decode(Int.self, forKey: .totalProPick) }
    catch { self.totalProPick = 0 }
    do { self.totalProWin = try container.decode(Int.self, forKey: .totalProWin) }
    catch { self.totalProWin = 0 }
    do { self.totalTurboPick = try container.decode(Int.self, forKey: .totalTurboPick) }
    catch { self.totalTurboPick = 0 }
    
    do { self.baseAtkMin = try container.decode(Int.self, forKey: .baseAtkMin) }
    catch { self.baseAtkMin = 0 }
    do { self.baseAtkMax = try container.decode(Int.self, forKey: .baseAtkMax) }
    catch { self.baseAtkMax = 0 }
    do { self.baseStr = try container.decode(Int.self, forKey: .baseStr) }
    catch { self.baseStr = 0 }
    do { self.baseAgi = try container.decode(Int.self, forKey: .baseAgi) }
    catch { self.baseAgi = 0 }
    do { self.baseInt = try container.decode(Int.self, forKey: .baseInt) }
    catch { self.baseInt = 0 }
    do { self.baseHealth = try container.decode(Int.self, forKey: .baseHealth) }
    catch { self.baseHealth = 0 }
    do { self.baseMana = try container.decode(Int.self, forKey: .baseMana) }
    catch { self.baseMana = 0 }
    do { self.baseManaRegen = try container.decode(Int.self, forKey: .baseManaRegen) }
    catch { self.baseManaRegen = 0 }
    do { self.baseArmor = try container.decode(Int.self, forKey: .baseArmor) }
    catch { self.baseArmor = 0 }
    do { self.baseMoveSpeed = try container.decode(Int.self, forKey: .baseMoveSpeed) }
    catch { self.baseMoveSpeed = 0 }
    do { self.baseAtkRange = try container.decode(Int.self, forKey: .baseAtkRange) }
    catch { self.baseAtkRange = 0 }
    do { self.baseProjectileSpeed = try container.decode(Int.self, forKey: .baseProjectileSpeed) }
    catch { self.baseProjectileSpeed = 0 }
    
    do { self.strGain = try container.decode(Double.self, forKey: .strGain) }
    catch { self.strGain = 0.0 }
    do { self.agiGain = try container.decode(Double.self, forKey: .agiGain) }
    catch { self.agiGain = 0.0 }
    do { self.intGain = try container.decode(Double.self, forKey: .intGain) }
    catch { self.intGain = 0.0 }
    
    do { self.baseHealthRegen = try container.decode(Double.self, forKey: .baseHealthRegen) }
    catch { self.baseHealthRegen = 0.0 }
    do { self.baseTurnRate = try container.decode(Double.self, forKey: .baseTurnRate) }
    catch { self.baseTurnRate = 0.0 }
    do { self.baseAttackRate = try container.decode(Double.self, forKey: .baseAttackRate) }
    catch { self.baseAttackRate = 0.0 }
    
    do { self.rawName = try container.decode(String.self, forKey: .rawName) }
    catch { self.rawName = "" }
    do { self.displayName = try container.decode(String.self, forKey: .displayName) }
    catch { self.displayName = "" }
    do { self.icon = try container.decode(String.self, forKey: .icon) }
    catch { self.icon = "" }
    do { self.image = try container.decode(String.self, forKey: .image) }
    catch { self.image = "" }
    do { self.attackType = try container.decode(String.self, forKey: .attackType) }
    catch { self.attackType = "" }
    do { self.primaryAttribute = try container.decode(String.self, forKey: .primaryAttribute) }
    catch { self.primaryAttribute = "" }
    
    do { self.roles = try container.decode(Array.self, forKey: .roles) }
    catch { self.roles = [] }
    
    var strRoleDisplayed = ""
    
    for role in self.roles {
      if strRoleDisplayed.isEmpty {
        strRoleDisplayed = role
      } else {
        strRoleDisplayed.append(", \(role)")
      }
    }
    
    self.rolesDisplayed = strRoleDisplayed
  }
}
